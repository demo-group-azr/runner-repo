import requests

# Define the URL of the API
url = "http://maps.googleapis.com/maps/api/geocode/json"

# Make a GET request to the API
response = requests.get(url)

# Check if the request was successful
if response.status_code == 200:
    # Print the response content
    print(response.content)
else:
    # Print the error message
    print(f"Error: {response.status_code}")
